# NTHU-Introduction to Massive Data Analysis

## 考古題
https://docs.google.com/document/d/1DOtSLNnFwQePGwnfT3fd07J5U_rvEEpx3JdH9OuELAs/edit?usp=sharing

## TroubleShooting
Q: 

    WARNING: An illegal reflective access operation has occurred
    WARNING: Illegal reflective access by org.apache.spark.unsafe.Platform (file:/usr/local/lib/python3.7/site-packages/pyspark/jars/spark-unsafe_2.11-2.4.4.jar) to method java.nio.Bits.unaligned()
    WARNING: Please consider reporting this to the maintainers of org.apache.spark.unsafe.Platform
    WARNING: Use --illegal-access=warn to enable warnings of further illegal reflective access operations
    WARNING: All illegal access operations will be denied in a future release
    19/10/11 19:58:03 WARN NativeCodeLoader: Unable to load native-hadoop library for your platform... using builtin-java classes where applicable

A: (In mac)

    Use java 8
    "export JAVA_HOME=`/usr/libexec/java_home -v 1.8`"
    
Q: 

    Exception: Python in worker has different version 2.7 than that in driver 3.7, PySpark cannot run with different minor versions.Please check environment variables PYSPARK_PYTHON and PYSPARK_DRIVER_PYTHON are correctly set.

A: (In mac) 
    
    nano ~/.bash_profile
    add the following code in .bash_profile
        export PYSPARK_PYTHON=python3
        export PYSPARK_DRIVER_PYTHON=ipython3
    source ~/.bash_profile
    

## HW1 - Matrix multiplication
### Demand
[Click me](./MDA_hw1/MDA_HW1.pdf)

### Solution
1. 透過 map 函數構建以左矩陣的行號(j)和右矩陣的列號(j)作為 key 的數據結構。即 (j, (i,L_val)) 和 (j, (k,R_val))。
2. 對上一步驟進行 join 運算得到 (j, [(i,L_val), (k,R_val)])。
3. 對上一步驟的值通過 map 函數構建以左矩陣的列號(i)和右矩陣的行號(k)作為 key , 且對應元素的積作為 value 的數據結構。即 ((i,k), L_val*R_val)。
4. 對上一步驟進行 reduceByKey, 對 (i,k) 相同的兩兩結果進行相加即得到第i列第k行的結果, 即 P_ik。

### Requirements
- [Python 3](https://www.python.org/)
- [Pyspark](https://spark.apache.org/downloads.html)

### Usage
    git clone https://gitlab.com/warren30815/nthu-introduction-to-massive-data-analysis
    cd nthu-introduction-to-massive-data-analysis/MDA_hw1
    python3 MDA_hw1.py
    
## HW2 - PageRank
### Demand
[Click me](./MDA_hw2/MDA_HW2.pdf)

### Result
![](./MDA_hw2/result.png)

### Usage
    git clone https://gitlab.com/warren30815/nthu-introduction-to-massive-data-analysis
    cd nthu-introduction-to-massive-data-analysis/MDA_hw2
    python3 pagerank.py
    
### Requirements
- [Python 3](https://www.python.org/)
- [Pyspark](https://spark.apache.org/downloads.html)

### Solution
1. 先將原始數據整理成[(page1, outer link tuple), (page2, outer link tuple), (page3, outer link tuple)....]，命名為pages。
2. 用map建立一個紀錄PR_score的RDD，並賦予初始score為1 / NUM_NODES。
3. 用map複製一份上一步驟的PR_score，命名為last_round_PR_score，功能為紀錄上一輪的PR RDD，才能做差值計算。
4. 將步驟一的pages RDD進行解析，得到一個平坦的 '得分集合'，RDD單元的形式是（誰，獲得了多少PR score），並經由reduce相同的key，更新PR_score RDD。
5. 根據公式：BETA * x + (1 - BETA) / NUM_NODES，修正PR_score RDD，本作業BETA取值0.8。
6. 計算計算dead_ends，如果有發生，根據作業的pdf上的公式修正score。
7. 迭代至各項PR_score差值（目前 vs 上一個epoch）小於MIN_DELTA停止，實作上MIN_DELTA取值 = 1e-15，因為作業要求至少迭代20次。
8. 如還沒收斂，以本輪結果更新上一輪的PR_score RDD，以利計算差值。

## HW3 - KMeans
### Demand
[Click me](./MDA_hw3/MDA_HW3.pdf)

### Result
Please see [this doc](https://docs.google.com/document/d/1-oZf2UvKanJt6WabIjumuMQBUpWB4Ud9kLncwjktTBI/edit?usp=sharing)

### Usage
    git clone https://gitlab.com/warren30815/nthu-introduction-to-massive-data-analysis
    cd nthu-introduction-to-massive-data-analysis/MDA_hw3
    python3 KMeans.py

### Requirements
- [Python 3](https://www.python.org/)
- [Pyspark](https://spark.apache.org/downloads.html)

### Solution
Mapper
    Input:
        1. original data
        2. global constant representing the list of centers

    Compute:
        the nearest center for each data instance

    Emit:
        nearest center (key) and points (value).

Reducer
    Input:
        center instance / coordinate (key) and points (value)
    
    Compute:
        the new centers based on clusters
    
    Emit:
        new centers

-> Provide the next epoch of K-Means with:
    1. the same data from your initial epoch
    2. the centers emitted from the reducer as global constants
    3. Repeat until 20 iterations are met.
    
## HW4 - LSH
### Demand
[Click me](./MDA_hw4/MDA_HW4.pdf)

### Result
Please see [this doc](https://docs.google.com/document/d/1lOVwBH5wh5ZhL14jwJAX-w9YPEibQJ_lFwwNv8QC_oo/edit?usp=sharing)

### Usage
    git clone https://gitlab.com/warren30815/nthu-introduction-to-massive-data-analysis
    cd nthu-introduction-to-massive-data-analysis/MDA_hw4
    python3 LSH.py

### Requirements
- [Python 3](https://www.python.org/)
- [Pyspark](https://spark.apache.org/downloads.html)

### Solution
[MapReduce Reference](https://stackoverflow.com/questions/29320943/how-to-implement-lsh-by-mapreduce)

## Term project - TrustWalker
### Report
https://docs.google.com/document/d/1AhxJjDZSUwzT_Ou1JLGLr7isP1jIWHsaUdmG_7MWPkM/edit?usp=sharing

### Usage
    git clone https://gitlab.com/warren30815/nthu-introduction-to-massive-data-analysis
    cd nthu-introduction-to-massive-data-analysis/MDA_term_project/
    python3 main.py
    
### Notes
Mac安裝流程：
1. on terminal type $ brew install apache-spark
2. if you see this error message, enter $ brew cask install caskroom/versions/java8 to install Java8, you will not see this error if you have it already installed.
3. check if pyspark is properly install by typing on the terminal $ pyspark. If you see the below it means that it has been installed properly:
![](https://gdcoder.com/content/images/2019/04/Screen-Shot-2019-05-01-at-00.08.08.png)