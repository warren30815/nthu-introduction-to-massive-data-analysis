#!/usr/bin/env python
# coding: utf-8

from pyspark import SparkConf, SparkContext

def row_key_mapper(line):
	maplist = line.split(",")
	return (int(maplist[1]), (int(maplist[2]), int(maplist[3])))

def column_key_mapper(line):
	maplist = line.split(",")
	return (int(maplist[2]), (int(maplist[1]), int(maplist[3])))

def reducer(x,y):
	return x+y

conf = SparkConf().setMaster("local").setAppName("MDA_hw1")
sc = SparkContext(conf = conf)
text_m_rdd = sc.textFile("500input.txt").filter(lambda x : x.find('M') > -1)
text_n_rdd = sc.textFile("500input.txt").filter(lambda x : x.find('N') > -1)
m_words = text_m_rdd.map(column_key_mapper)
n_words = text_n_rdd.map(row_key_mapper)

output = m_words.join(n_words).values().map(lambda x: ((x[0][0], x[1][0]), x[0][1] * x[1][1])).reduceByKey(reducer).map(lambda x: (x[0][0], x[0][1], x[1]))
sorted_output = output.sortBy(lambda x: (x[0], x[1])).collect()
with open('result.txt','w', encoding='UTF-8') as file:
	for line in sorted_output:
		for element in line:
			file.write(str(element) + " ")
		file.write('\n')
print("Done")



