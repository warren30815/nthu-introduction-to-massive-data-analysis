# import os
# import binascii

import sys
import os.path
import string
import os
import re
import random
import time
import binascii
from pyspark import SparkContext, SparkConf 

conf = SparkConf().setMaster("local").setAppName("MDA_hw4")
sc = SparkContext(conf = conf)

# Define a function to map a 2D matrix coordinate into a 1D index.
def getTriangleIndex(i, j):
    # If i == j that's an error.
    if i == j:
        sys.stderr.write("Can't access triangle matrix with i == j")
        sys.exit(1)
    # If j < i just swap the values.
    if j < i:
        temp = i
        i = j
        j = temp

    # Calculate the index within the triangular array.
    # This fancy indexing scheme is taken from pg. 211 of:
    # http://infolab.stanford.edu/~ullman/mmds/ch6.pdf
    # But I adapted it for a 0-based index.
    # Note: The division by two should not truncate, it
    #       needs to be a float.
    k = int(i * (len(docsAsShingleSets) - (i + 1) / 2.0) + j - i) - 1

    return k

# https://www.codeproject.com/Articles/691200/Primality-test-algorithms-Prime-test-The-fastest-w
# check if integer n is a prime
# probabilistic method, much faster than usual priminality tests
def MillerRabinPrimalityTest(number):
    '''
    because the algorithm input is ODD number than if we get
    even and it is the number 2 we return TRUE ( spcial case )
    if we get the number 1 we return false and any other even
    number we will return false.
    '''
    if number == 2:
        return True
    elif number == 1 or number % 2 == 0:
        return False

    ''' first we want to express n as : 2^s * r ( were r is odd ) '''

    ''' the odd part of the number '''
    oddPartOfNumber = number - 1

    ''' The number of time that the number is divided by two '''
    timesTwoDividNumber = 0

    ''' while r is even divid by 2 to find the odd part '''
    while oddPartOfNumber % 2 == 0:
        oddPartOfNumber = oddPartOfNumber / 2
        timesTwoDividNumber = timesTwoDividNumber + 1

    '''
    since there are number that are cases of "strong liar" we
    need to check more then one number
    '''
    for time in range(3):

        ''' choose "Good" random number '''
        while True:
            ''' Draw a RANDOM number in range of number ( Z_number )  '''
            randomNumber = random.randint(2, number) - 1
            if randomNumber != 0 and randomNumber != 1:
                break

        ''' randomNumberWithPower = randomNumber^oddPartOfNumber mod number '''
        randomNumberWithPower = pow(randomNumber, int(oddPartOfNumber), number)

        ''' if random number is not 1 and not -1 ( in mod n ) '''
        if (randomNumberWithPower != 1) and (randomNumberWithPower != number - 1):
            # number of iteration
            iterationNumber = 1

            ''' while we can squre the number and the squered number is not -1 mod number'''
            while (iterationNumber <= timesTwoDividNumber - 1) and (randomNumberWithPower != number - 1):
                ''' squre the number '''
                randomNumberWithPower = pow(randomNumberWithPower, 2, number)

                # inc the number of iteration
                iterationNumber = iterationNumber + 1
            '''
            if x != -1 mod number then it because we did not found strong witnesses
            hence 1 have more then two roots in mod n ==>
            n is composite ==> return false for primality
            '''
            if (randomNumberWithPower != (number - 1)):
                return False

    ''' well the number pass the tests ==> it is probably prime ==> return true for primality '''
    return True


# Our random hash function will take the form of:
#   h(x) = (a*x + b) % c
# Where 'x' is the input value, 'a' and 'b' are random coefficients, and 'c' is
# a prime number just greater than shingleNo.

# Generate a list of 'k' random coefficients for the random hash functions,
# while ensuring that the same value does not appear multiple times in the
# list.
def pickRandomCoeffs(k):
    # Create a list of 'k' random values.
    randList = []

    while k > 0:
        # Get a random shingle ID.
        randIndex = random.randint(0, maxShingleID)

        # Ensure that each random number is unique.
        while randIndex in randList:
            randIndex = random.randint(0, maxShingleID)

            # Add the random number to the list.
        randList.append(randIndex)
        k = k - 1

    return randList

# =============================================================================
#               Read Files & Preprocess
# =============================================================================

shingle_size = 3
numHashes = 100
neighbors = 101
row_num_per_band = 2
threshold = 0

document_dict = {}
document_index = 0
t = {}

for file in os.listdir("athletics/"):
    if file.endswith(".txt"):
        filename = os.path.join("athletics", file)
        f = open(filename, 'r')
        document_dict[document_index] = f.read().replace("\n", " ")
        document_index += 1

for index in range(len(document_dict)):
    document_dict[index] = document_dict[index].split()

# =============================================================================
#               Convert Documents To Sets of Shingles
# =============================================================================

docsAsShingleSets = {}

docNames = []

totalShingles = 0
shingleNo = 0

print("Shingling articles...")

t0 = time.time()
# loop through all the documents
for i in range(0, len(document_dict)):

    # Read all of the words (they are all on one line)
    words = document_dict[i]

    # Retrieve the article ID
    docID = i

    # Maintain a list of all document IDs.
    docNames.append(docID)

    # 'shinglesInDoc' will hold all of the unique shingles present in the
    # current document. If a shingle ID occurs multiple times in the document,
    # it will only appear once in the set.

    # keep word shingles
    shinglesInDocWords = set()

    # keep hashed shingles
    shinglesInDocInts = set()

    shingle = []
    # For each word in the document...
    for index in range(len(words) - shingle_size + 1):
        # Construct the shingle text by combining k words together.
        shingle = words[index:index + shingle_size]
        shingle = ' '.join(shingle)

        # Hash the shingle to a 32-bit integer.
        crc = binascii.crc32(shingle.encode()) & 0xffffffff

        if shingle not in shinglesInDocWords:
            shinglesInDocWords.add(shingle)
        # Add the hash value to the list of shingles for the current document.
        # Note that set objects will only add the value to the set if the set
        # doesn't already contain it.

        if crc not in shinglesInDocInts:
            shinglesInDocInts.add(crc)
            # Count the number of shingles across all documents.
            shingleNo = shingleNo + 1
        else:
            del shingle
            index = index - 1

    # Store the completed list of shingles for this document in the dictionary.
    docsAsShingleSets[docID] = shinglesInDocInts

totalShingles = shingleNo

print('Total Number of Shingles', shingleNo)
# Report how long shingling took.
print('\nShingling ' + str(len(docsAsShingleSets)) + ' docs took %.2f sec.' % (time.time() - t0))

print('\nAverage shingles per doc: %.2f' % (shingleNo / len(docsAsShingleSets)))

# =============================================================================
#                     Define Triangle Matrices
# =============================================================================

# Define virtual Triangle matrices to hold the similarity values. For storing
# similarities between pairs, we only need roughly half the elements of a full
# matrix. Using a triangle matrix requires less than half the memory of a full
# matrix. Using a triangle matrix requires less than half the memory of a full
# matrix, and can protect the programmer from inadvertently accessing one of
# the empty/invalid cells of a full matrix.

# Calculate the number of elements needed in our triangle matrix
numElems = int(len(docsAsShingleSets) * (len(docsAsShingleSets) - 1) / 2)

# Initialize two empty lists to store the similarity values.
# 'JSim' will be for the actual Jaccard Similarity values.
# 'estJSim' will be for the estimated Jaccard Similarities found by comparing
# the MinHash signatures.
JSim = [0 for x in range(numElems)]
estJSim = [0 for x in range(numElems)]


print('\nGenerating random hash functions...')

# =============================================================================
#                 Generate MinHash Signatures
# =============================================================================
from time import clock

# Time this step.
t0 = time.time()

# Record the total number of shingles
i = 1
# find first prime which is higher than the total number of shingles
# print 'Total number of shingles = ', shingleNo
while not MillerRabinPrimalityTest(shingleNo + i):
    i = i + 1
# print 'Next prime = ', shingleNo + i

maxShingleID = shingleNo
nextPrime = shingleNo + i

# For each of the 'numHashes' hash functions, generate a different coefficient 'a' and 'b'.
coeffA = pickRandomCoeffs(numHashes)
coeffB = pickRandomCoeffs(numHashes)

print('\nGenerating MinHash signatures for all documents...')

# List of documents represented as signature vectors
signatures = []

# Rather than generating a random permutation of all possible shingles,
# we'll just hash the IDs of the shingles that are *actually in the document*,
# then take the lowest resulting hash code value. This corresponds to the index
# of the first shingle that you would have encountered in the random order.
# For each document...
for docID in docNames:

    # Get the shingle set for this document.
    shingleIDSet = docsAsShingleSets[docID]

    # The resulting minhash signature for this document.
    signature = []

    # For each of the random hash functions...
    for i in range(0, numHashes):

        # For each of the shingles actually in the document, calculate its hash code
        # using hash function 'i'.

        # Track the lowest hash ID seen. Initialize 'minHashCode' to be greater than
        # the maximum possible value output by the hash.
        minHashCode = nextPrime + 1

        # For each shingle in the document...
        for shingleID in shingleIDSet:
            # Evaluate the hash function.
            hashCode = (coeffA[i] * shingleID + coeffB[i]) % nextPrime

            # Track the lowest hash code seen.
            if hashCode < minHashCode:
                minHashCode = hashCode

        # Add the smallest hash code value as component number 'i' of the signature.
        signature.append(minHashCode)

    # Store the MinHash signature for this document.
    signatures.append(signature)

# Calculate the elapsed time (in seconds)
elapsed = (time.time() - t0)

print("\nGenerating MinHash signatures took %.2fsec" % elapsed)

# =============================================================================
#                                   LSH
# =============================================================================

# https://github.com/anthonygarvan/MinHash
from random import randint, choice, random
import string
import sys
import itertools

def get_band_hashes(minhash_row, row_num_per_band):
    band_hashes = []
    for i in range(len(minhash_row)):
        if i % row_num_per_band == 0:
            if i > 0:
                band_hashes.append(band_hash)
            band_hash = 0
        band_hash += hash(minhash_row[i])
    return band_hashes

result = []
NUM_OUTPUT = 10

def get_similar_docs(docs, docid, shingles, threshold, n_hashes, row_num_per_band, collectIndexes=True):
    t0 = time.time()
    lshsignatures = {}
    hash_bands = {}
    random_strings = [str(random()) for _ in range(n_hashes)]
    docNum = 0
    w = 0
    neighbors_of_given_documentLSH = {}

    for doc in docs:

        lshsignatures[w] = doc

        minhash_row = doc

        band_hashes = get_band_hashes(minhash_row, row_num_per_band)

        w = w + 1
        docMember = docNum if collectIndexes else doc
        for i in range(len(band_hashes)):
            if i not in hash_bands:
                hash_bands[i] = {}
            if band_hashes[i] not in hash_bands[i]:
                hash_bands[i][band_hashes[i]] = [docMember]
            else:
                hash_bands[i][band_hashes[i]].append(docMember)
        docNum += 1

    similar_docs = set()
    similarity1 = []
    noPairs = 0
    print('Comparing Signatures Found in the Same Buckets During LSH ...')

    samebucketLSH = []
    samebucketcnt = 0
    for i in hash_bands:
        for hash_num in hash_bands[i]:
            if len(hash_bands[i][hash_num]) > 1:
                # print(hash_bands[i][hash_num])
                for pair in itertools.combinations(hash_bands[i][hash_num], r=2):
                    if pair not in similar_docs:
                        similar_docs.add(pair)
                        if pair[0] == docid and pair[1] != docid:

                            s1 = set(lshsignatures[pair[0]])
                            s2 = set(lshsignatures[pair[1]])

                            sim = len(s1.intersection(s2)) / float(len(s1.union(s2)))
                            if (float(sim) > threshold):
                                percsim = sim * 100
                                # print  "  %5s --> %5s   %.2f%s" % (pair[0], pair[1], percsim,'%')
                                noPairs = noPairs + 1
                                # return similar texts

                                # print 'TEXT WITH ID: ', pair[0], '\n AND BODY: ', body[pair[0]], '\n IS ', sim*100, '% SIMILAR TO', '\n TEXT WITH ID: ', pair[1], '\n AND BODY: ', body[pair[1]], '\n'
                            else:
                                percsim = 0
                            neighbors_of_given_documentLSH[pair[1]] = percsim
                            elapsed = (time.time() - t0)

    sorted_neigborsLSH = sorted(neighbors_of_given_documentLSH.items(), key=lambda x: x[1], reverse=True)

    # print("The " + str(neighbors) + " closest neighbors of document " + str(docid) + " are:")
    for i in range(0, neighbors):
        if i >= len(sorted_neigborsLSH):
            break
        if sorted_neigborsLSH[i][1] > 0:
            result.append((docid, str(sorted_neigborsLSH[i][0]), str(round(sorted_neigborsLSH[i][1], 2))))
            # print("\nChosen Signatures (After LSH) of Document " + str(sorted_neigborsLSH[i][0]) + " with Jaccard Similarity " + str(round(sorted_neigborsLSH[i][1], 2)) + "%")


def run(docid):
    n_hashes = numHashes
    finalshingles = docsAsShingleSets
    get_similar_docs(signatures, docid, finalshingles, threshold, n_hashes, row_num_per_band, collectIndexes=True)

for docid in range(len(document_dict)):
    run(docid)

result_rdd = sc.parallelize(result)

counter = 1
for pair in result_rdd.sortBy(lambda x: x[2], ascending=False).collect():
    if(counter <= NUM_OUTPUT):
        former, later = int(pair[0]), int(pair[1])
        score = pair[2]
        print("Document: " + str(former+1) + " & " + str(later+1))
        print("Jaccard_similarity: " + str(score) + "%")
        print("-----")
        counter += 1
    else:
        break