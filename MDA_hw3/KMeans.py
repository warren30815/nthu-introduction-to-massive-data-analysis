#!/usr/bin/env python
# coding: utf-8

from pyspark import SparkContext, SparkConf 
import math
import matplotlib.pyplot as plt
import random
import numpy as np
import sys 
import csv

MODE = 'e' # e(euclidean) or m(manhattan)

k = 10
col = 58
iteration = 20

conf = SparkConf().setMaster("local").setAppName("MDA_hw3")
sc = SparkContext(conf = conf)

def getEuclidean(point1, point2):
    dimension = col
    dist = 0.0
    for i in range(dimension):
        dist += (point1[i] - point2[i]) ** 2
    return dist, math.sqrt(dist)

def getManhattan(point1, point2):
    dimension = col
    dist = 0.0
    for i in range(dimension):
        dist += abs(point1[i] - point2[i])
    return dist, dist

def classifier(item):
    global vectors
    dist_array = np.zeros(k)
    cost_array = np.zeros(k)
    for i, point in enumerate(vectors):
        if (MODE == "e"):
            cost_array[i], dist_array[i] = getEuclidean(item, point)
        elif (MODE == "m"):
            cost_array[i], dist_array[i] = getManhattan(item, point)
    classIndex = dist_array.argmin()
    cost = cost_array.min()
    return (classIndex, (item, cost))

def update_centroids(items):
    clusterHeart = []

    for i in range(col):
        clusterHeart.append(0)

    for content in items[1]:
        for i, coordinate in enumerate(content):
            clusterHeart[i] += coordinate / len(items[1])
    return (items[0], clusterHeart)

def compute_init_loss(dataset):
    print("compute_init_loss")
    global vectors
    cost = 0.0
    cost_array = np.zeros(k)
    for data in dataset:
        for i, point in enumerate(vectors):
            if (MODE == "e"):
                cost_array[i], _ = getEuclidean(data, point)
            elif (MODE == "m"):
                cost_array[i], _ = getManhattan(data, point)
        cost += cost_array.min()
    return cost

def init_centroid(dataset, k):
    print("init_centroid")

    global vectors # 簇心向量
    vectors = []
    for centroid in dataset:
        vectors.append(centroid)

def k_means(dataset, k, iteration, INIT_filename):
    global vectors # 簇心向量
    costs = np.zeros(iteration + 1)

    init_dataset = np.loadtxt(INIT_filename + ".txt", usecols=range(0, col), delimiter=' ', dtype=np.float32)
    init_centroid(init_dataset, k)
    costs[0] = compute_init_loss(dataset)

    Data = sc.parallelize([item for item in dataset])
    
    #根據迭代次數重複k-means聚類過程
    for i in range(1, iteration + 1):
        print(str(i) + " round start")
        # 簇 : [(cluster_index, [items])]
        C = Data.map(classifier).reduceByKey(lambda x, y: x+y).collect()

        for _, result in C:
            for index in range(1, len(result), 2):
                cost = result[index]
                costs[i] += cost

        clear_C = []
        for cluster_index, result in C:
            tmp = []
            for index in range(0, len(result), 2):
                content = result[index]
                tmp.append(content)
            clear_C.append((cluster_index, tuple(tmp)))

        C = sc.parallelize(clear_C)
        new_centroids = C.map(update_centroids).collect()
        for content in new_centroids:
            cluster_index = content[0]
            vectors[cluster_index] = content[1]

    improvement = (costs[0] - costs[-1]) / costs[0] * 100.
    costs = costs[1:]
    return costs, vectors, improvement

def plot_losses(png_save_path, c1_losses, c2_losses, max_iter, method):
    x = ['Round {}'.format(i) for i in range(1, max_iter + 1)]
    fig = plt.figure(0)
    plt.title(method)
    plt.plot(x, c1_losses, label='c1')
    plt.plot(x, c2_losses, label='c2')
    plt.legend(loc='upper right')
    plt.savefig(png_save_path)
    plt.close(0)

def record_centroid_pair_distance_in_csv(csv_file_path, centroids, method):
    with open(csv_file_path, "w", newline='') as csvfile:
        centroid_num = len(centroids)
        writer = csv.writer(csvfile)
        writer.writerow([method] + list(range(1, centroid_num + 1)))
        for i in range(centroid_num):
            row = [i + 1]
            for j in range(centroid_num):
                if i > j:
                    row += ['']
                    continue
                elif i == j:
                    value = 0.00
                else:
                    if method == 'e': # Euclidean
                        value = np.sqrt(np.sum(np.power(np.array(centroids[i]) - np.array(centroids[j]), 2)))
                    elif method == 'm': # Manhattan
                        value = np.sum(np.abs(np.array(centroids[i]) - np.array(centroids[j])))
                    else:
                        raise NameError
                row += [np.round(value, 2)]
            writer.writerow(row)

if __name__== "__main__":
    global vectors
    dataset = np.loadtxt("data.txt", usecols=range(0, col), delimiter=' ', dtype=np.float32)

    EUCLIDEAN_METHOD = 'e'
    MANHATTAN_METHOD = 'm'
    METHODS = [EUCLIDEAN_METHOD, MANHATTAN_METHOD]

    pc_improvements = []

    for METHOD in METHODS:
        MODE = METHOD

        print('\nc1\n')
        c1_losses, c1_centroids, c1_improvement = k_means(dataset, k, iteration, "c1")

        print('\nc2\n')
        c2_losses, c2_centroids, c2_improvement = k_means(dataset, k, iteration, "c2")

        plot_losses('results/{}.png'.format(METHOD), c1_losses, c2_losses, iteration, METHOD)

        record_centroid_pair_distance_in_csv('results/{}_c1_{}.csv'.format(METHOD, EUCLIDEAN_METHOD), c1_centroids, EUCLIDEAN_METHOD)
        record_centroid_pair_distance_in_csv('results/{}_c1_{}.csv'.format(METHOD, MANHATTAN_METHOD), c1_centroids, MANHATTAN_METHOD)
        record_centroid_pair_distance_in_csv('results/{}_c2_{}.csv'.format(METHOD, EUCLIDEAN_METHOD), c2_centroids, EUCLIDEAN_METHOD)
        record_centroid_pair_distance_in_csv('results/{}_c2_{}.csv'.format(METHOD, MANHATTAN_METHOD), c2_centroids, MANHATTAN_METHOD)

        pc_improvements += [c1_improvement, c2_improvement]

    print(pc_improvements)