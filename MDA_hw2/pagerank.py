#!/usr/bin/env python
# coding: utf-8

from pyspark import SparkContext, SparkConf 

# region 常數區
NUM_NODES = 10879 
BETA = 0.8
NUM_OUTPUT = 10
MAX_ITER = 100  # 最大迭代次數
MIN_DELTA = 1e-15  # 如差值小於此，則結束迭代
# endregion

def preprocess_data(line):
    maplist = line.split('\t')
    return (maplist[0], (maplist[1],))

def flat(x):
    list1 = []
    s = len(x[1][0])
    for y in x[1][0]:
        list1.append(tuple((y, x[1][1]/s)))
    return list1

def compute_PR_diff(last_round, this_round):
    diff = 0.0
    last_round_dict = dict(last_round.collect())
    this_round_dict = dict(this_round.collect())   
    for key in this_round_dict:
        diff += abs(this_round_dict[key] - last_round_dict[key])
    return diff

if __name__== "__main__":

    conf = SparkConf().setMaster("local").setAppName("MDA_hw2")
    sc = SparkContext(conf = conf)
    text_rdd = sc.textFile("p2p-Gnutella04.txt") # p2p-Gnutella04
    
    pages = text_rdd.map(preprocess_data).reduceByKey(lambda x, y: x+y)
    # print(pages.collect())
    # 如sample data會整理成：[('1', ('2', '3')), ('2', ('4',)), ('3', ('1', '4', '5')), ('5', ('1', '4'))]

    # 初始pr值都設置為1 / NUM_NODES
    # range函數取值含下界不含上界
    # numSlices is to resolve the warning log: "Stage x contains a task of very large size (xxx KB). The maximum recommended task size is 100 KB"
    PR_score = sc.parallelize([str(i) for i in range(0, NUM_NODES)], numSlices=10).map(lambda x: (x, 1 / NUM_NODES))

    # 紀錄上一輪的PR RDD，才能做差值計算
    last_round_PR_score = PR_score.map(lambda x: x)

    # 迭代
    for i in range(MAX_ITER):
        # join後的pairRDD格式：(key,((連結到外部page的link list),PR))
        rank = pages.join(PR_score).flatMap(flat)
        # 對於上面得到的每個pairRDD的元組，flat函數將link list中的每一個link都計算了當前key的投票值即PR/s，獲得的新的RDD--rank
        # flatmap負責得到一個由各列表中的元素組成的RDD，而不是一個由列表組成的RDD。
        # rank可以理解為flatmap過後得到一個平坦的 '得分集合'，RDD單元的形式是（誰，獲得了多少PR score）
        # print(rank.collect())
        # 如sample data會整理成：[('2', 0.1), ('3', 0.1), ('4', 0.2), ('1', 0.06666666666666667), ('4', 0.06666666666666667), ('5', 0.06666666666666667), ('1', 0.1), ('4', 0.1)]

        # reduce
        PR_score = rank.reduceByKey(lambda x, y: x+y)

        # 修正score
        PR_score = PR_score.mapValues(lambda x: BETA * x + (1 - BETA) / NUM_NODES)

        print("This is NO.%s iteration" % (i + 1))

        # 計算dead_ends
        # 如果有發生，修正score
        if (len(pages.collect()) < NUM_NODES):
            total_old_reward = 0.0
            for _ , old_reward in PR_score.collect():
                total_old_reward += old_reward
            extra = 1 - total_old_reward
            PR_score = PR_score.mapValues(lambda x: x + extra / NUM_NODES)

        change = compute_PR_diff(last_round_PR_score, PR_score)
        # Cyan color
        print("\033[96m{}{}\033[00m".format("PR diff: ", change))

        if change < MIN_DELTA:
            # Green color
            print("\033[92m{}{}{}\033[00m".format("Converge in ", i+1, " iterations!")) 
            break

        # 以本輪結果更新上一輪的PR RDD
        last_round_PR_score = PR_score.map(lambda x: x)

    counter = 1
    with open('result.txt','w', encoding='UTF-8') as file:
        for (page_name, score) in PR_score.sortBy(lambda x: x[1], ascending= False).collect():
            if(counter <= NUM_OUTPUT):
                # Yellow color 
                print("\033[93m{}{}{}{}{}{}\033[00m".format("TOP", str(counter), ": ", str(page_name), ", PR= ", str(round(score,7))))
                file.write("TOP" + str(counter) + ": " + str(page_name) + ", PR= " + str(round(score,7)))
                file.write('\n')
                counter += 1
            else:
                break



